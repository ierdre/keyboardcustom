//
//  ViewController.swift
//  testCustomKeyaboard
//
//  Created by Thibaut LE LEVIER on 06/06/2019.
//  Copyright © 2019 Cogelec. All rights reserved.
//

import UIKit
class textfield: UITextField {
    var _inputViewController : UIInputViewController?
    override public var inputViewController: UIInputViewController?{
        get { return _inputViewController }
        set { _inputViewController = newValue }
    }
}
class ViewController: UIViewController {

    @IBOutlet weak var textEntree: textfield!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       /*
 InputViewController *inputController = [[InputViewController alloc] init];
 inputController.didChangeDate = ^(NSDate *date) {
 self.textField.text = [NSDateFormatter localizedStringFromDate:date dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
 };
 
 self.textField.inputAccessoryView = self.accessoryToolbar;
 self.textField.inputViewController = inputController;*/
            var keyb = KeyboardViewController()
        self.textEntree.inputViewController = keyb
        
        
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: Selector("doneButtonAction"))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        self.textEntree.inputAccessoryView = toolbar
    }

    @objc func doneButtonAction() {
        self.view.endEditing(true)
    }
}

