//
//  KeyboardViewController.swift
//  intratoneKeyboard
//
//  Created by Thibaut LE LEVIER on 06/06/2019.
//  Copyright © 2019 Cogelec. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController {
    var calculatorView: UIView!
    @IBOutlet var nextKeyboardButton: UIButton!
    var proxy : UITextDocumentProxy!
    @IBAction func testClick(_ sender: Any) {
        if let button = sender as? UIButton {
            print (" - ")
            proxy.insertText("\((button.titleLabel?.text)!)")
        }
    }
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        // Add custom view sizing constraints here
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
         self.inputView?.translatesAutoresizingMaskIntoConstraints = false
        
         loadInterface()
        
        proxy = textDocumentProxy as UITextDocumentProxy
        
        
            for _: Int in 0 ..< 20 {
                proxy.deleteBackward()
            }
        
    }
    
    override func viewWillLayoutSubviews() {
        self.nextKeyboardButton.isHidden = !self.needsInputModeSwitchKey
        super.viewWillLayoutSubviews()
    }
    
    override func textWillChange(_ textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
        print(" OK " )
    }
    
    override func textDidChange(_ textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
        print(" OK " )
        
    }
    
    
    func loadInterface() {
        // load the nib file
        var calculatorNib = UINib(nibName: "keyboardCustom", bundle: nil)
        // instantiate the view
        calculatorView = calculatorNib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        // add the interface to the main view
        view.addSubview(calculatorView)
        
        // copy the background color
        view.backgroundColor = calculatorView.backgroundColor
        
    }

}
